#coding:utf-8
import os
import sys
import datetime


sys.path.append(os.path.expanduser('wsd'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'wsd.settings'

from Main.models import *
from django.db import transaction

female_gender = Gender.objects.get(name_en="Female")
male_gender = Gender.objects.get(name_en="Male")

kazakh_nationality = Nationality.objects.get(name_en="Kazakh")
russian_nationality = Nationality.objects.get(name_en="Russian")
kazakh_language = Language.objects.get(name="Kazakh")
russian_language = Language.objects.get(name="Russian")
english_language = Language.objects.get(name="English")
almaty_city = City.objects.get(name_en="Almaty")
astana_city = City.objects.get(name_en="Astana")

administrator_role = Role.objects.get(name_en="Administrator")
student_role = Role.objects.get(name_en="Student")
tutor_role = Role.objects.get(name_en="Tutor")
parent_role = Role.objects.get(name_en="Parent")
staff_role = Role.objects.get(name_en="Staff")
tutor_staff_role = Role.objects.get(name_en="Tutor/Staff")

scientific_degree = ScientificDegree.objects.get(id=1)
academic_degree = AcademicDegree.objects.get(id=1)

tutor_status_work = TutorStatus.objects.get(id=1)
student_status_study = StudentStatus.objects.get(id=1)

worker_job = Job.objects.get(id=1)


def add_admin():
    user_admin = User.objects.create(username="admin",
                                     first_name=u"Админ",
                                     last_name=u"Админ")
    user_admin.set_password("admin")
    user_admin.save()
    admin_site_user = SiteUser.objects.create(
        user=user_admin,
        patronymic=u"Админский",
        birth_date=datetime.date(2003, 02, 01),
        address=Address.objects.create(
            street=u"Админская", number="34"
        ),
        gender=male_gender,
        nationality=kazakh_nationality,
        is_married=False,
        education=u"Высшее",
        city=almaty_city,
        role=administrator_role,
        language=russian_language
    )



def add_tutors():
    for i in range(10):
        user = User.objects.create(username="tutor" + str(i),
                                         first_name=u"Тутор"+str(i),
                                         last_name=u"Тутуровский"+str(i))
        user.set_password("123")
        user.save()
        site_user = SiteUser.objects.create(
            user=user,
            patronymic=u"Туторов"+str(i),
            birth_date=datetime.date(2003, 02, 01),
            address=Address.objects.create(
                street=u"Туторская", number="20"+str(i)
            ),
            gender=male_gender,
            nationality=kazakh_nationality,
            is_married=False,
            education=u"Высшее",
            city=almaty_city,
            role=tutor_role,
            language=russian_language
        )



        tutor = Tutor.objects.create(
            id=i+1,
            site_user=site_user,
            #cafedra,
            information_en="Tutor number " + str(i),
            information_ru="Препод номер " + str(i),
            information_kz="Препод номер " + str(i),
            scientific_degree=scientific_degree,
            academic_degree=academic_degree,
            rate=0.5,
            status=tutor_status_work,
            start_date=datetime.date(2000, 01, 01),
            length_of_work=2,
            job=worker_job
        )







def add_faculties():
    faculty = Faculty.objects.create(
        name_ru="Факультет общей медицины",
        name_kz="Факультет общей медицины",
        name_en="Faculty of common medicine",
        information_ru="Информация о Факультете общей медицины",
        information_kz="Информация о Факультете общей медицины",
        information_en="Information about faculty of common medicine",
        dean=Tutor.objects.get(id=1)
    )

    faculty = Faculty.objects.create(
        name_ru="Стоматологический факульет",
        name_kz="Стоматологический факульет",
        name_en="Stomatalogy faculty",
        information_ru="Информация о стоматологическом факульете",
        information_kz="Информация о стоматологическом факульете",
        information_en="Information about stomatalogy faculty",
        dean=Tutor.objects.get(id=2)
    )


def add_buildings():
    Building.objects.create(
        id=1,
        address=Address.objects.create(
            street=u"sampleStreet",
            number=u"777"
        ),
        information_en=u"Info",
        information_ru=u"Инфо",
        information_kz=u"Инфо"

    )


def add_cafedras():
    cafedra = Cafedra.objects.create(
        name_en="Cafedra gistology",
        name_ru="Кафедра гистологии",
        name_kz="Кафедра гистологии",
        cafedra_head=Tutor.objects.get(id=3),
        faculty=Faculty.objects.get(name_en="Faculty of common medicine"),
        information_en="info Cafedra gistology",
        information_ru="инфа Кафедра гистологии",
        information_kz="инфа Кафедра гистологии",
        building=Building.objects.get(id=1)
    )

    cafedra = Cafedra.objects.create(
        name_en="Cafedra biology",
        name_ru="Кафедра биологии",
        name_kz="Кафедра биологии",
        cafedra_head=Tutor.objects.get(id=4),
        faculty=Faculty.objects.get(name_en="Faculty of common medicine"),
        information_en="Info Cafedra biology",
        information_ru="Info Кафедра биологии",
        information_kz="Info Кафедра биологии",
        building=Building.objects.get(id=1)
    )

    cafedra = Cafedra.objects.create(
        name_en="Cafedra ortopedy",
        name_ru="Кафедра ортопедии",
        name_kz="Кафедра ортопедии",
        cafedra_head=Tutor.objects.get(id=4),
        faculty=Faculty.objects.get(name_en="Stomatalogy faculty"),
        information_en="Info Cafedra ortopedy",
        information_ru="Info Кафедра ортопедии",
        information_kz="Info Кафедра ортопедии",
        building=Building.objects.get(id=1)
    )


def add_specialties():
    specialty = Specialty.objects.create(
        name_en="Common medicine",
        name_ru="Общая медицина",
        name_kz="Общая медицина",
        code="5B130110",
        faculty=Faculty.objects.get(name_en="Faculty of common medicine"),
        information_en="Info Common medicine",
        information_ru="Info Общая медицина",
        information_kz="Info Общая медицина"
    )
    specialty = Specialty.objects.create(
        name_en="Stomatology",
        name_ru="Стоматлогия",
        name_kz="Стоматлогия",
        code="5B130200",
        faculty=Faculty.objects.get(name_en="Stomatalogy faculty"),
        information_en="Info Stomatology",
        information_ru="Инфо Стоматлогия",
        information_kz="ИНфо Стоматлогия",
    )


def add_directions():
    Direction.objects.create(
        name_en="Main direction",
        name_kz="Основное направление",
        name_ru="Основное направление",
        information_en="Info Main direction",
        information_kz="Инфо Основное направление",
        information_ru="Инфо Основное направление",
        specialty=Specialty.objects.get(name_en="Stomatology")
    )
    Direction.objects.create(
        name_en="Surgery",
        name_kz="Хирургия",
        name_ru="Хирургия",
        information_en="Info Surgery",
        information_kz="Инфо Хирургия",
        information_ru="Инфо Хирургия",
        specialty=Specialty.objects.get(name_en="Common medicine")
    )
    Direction.objects.create(
        name_en="Pediatrics",
        name_kz="Педиатрия",
        name_ru="Педиатрия",
        information_en="Info Pediatrics",
        information_kz="Инфо Педиатрия",
        information_ru="Инфо Педиатрия",
        specialty=Specialty.objects.get(name_en="Common medicine")
    )



if __name__ == "__main__":
    #with transaction.commit_manually():
        add_admin()
        add_tutors()
        add_faculties()
        add_buildings()
        add_cafedras()
        add_specialties()
        add_directions()
        #transaction.rollback()







