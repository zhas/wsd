#coding: utf-8
from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
                       url(r'^index/$', views.index_view, name="main_index"),
                       url(r'^login/$', views.login_view, name="main_login"),
                       url(r'^logout/$', views.logout_view, name="main_logout"),
                       url(r'^catalog/$', views.catalog_view, name="main_catalog"),
                       url(r'^change_language/$', views.change_language_view, name="main_change_language"),
                       url(r'^language_changed/$', views.language_changed_view, name="main_language_changed"),
                       # Faculties:
                       url(r'^faculty/list/$', views.faculty_list_view, name="main_faculty_list"),
                       url(r'^faculty/add/$', views.faculty_add_view, name="main_faculty_add"),
                       url(r'^faculty/edit/(?P<faculty_id>\d+)/$', views.faculty_edit_view, name="main_faculty_edit"),
                       url(r'^faculty/display/(?P<faculty_id>\d+)/$', views.faculty_display_view,
                           name="main_faculty_display"),
                       url(r'^faculty/delete/(?P<faculty_id>\d+)/$', views.faculty_delete_view,
                           name="main_faculty_delete"),
                       )