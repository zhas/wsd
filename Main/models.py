#coding:utf-8
from django.db import models
from django.contrib.auth.models import User
from transmeta import TransMeta


class Language(models.Model):
    name = models.TextField()
    code = models.TextField()


class TutorStatus(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class StudentStatus(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class StaffStatus(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class StudyForm(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class PaymentForm(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class StudyLanguage(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Activity(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Phone(models.Model):
    phone = models.CharField(max_length=50)
    description = models.TextField()


class AcademicDegree(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class ScientificDegree(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Address(models.Model):
    street = models.CharField(max_length=100)
    number = models.CharField(max_length=10)
    phones = models.ManyToManyField(Phone)#ONE TO MANY


class Job(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Weekday(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()
    order = models.IntegerField()

    class Meta:
        translate = ('name',)



class Nationality(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Gender(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Country(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class City(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)
    country = models.ForeignKey(Country)


class DormState(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class IDCard(models.Model):
    number = models.CharField(max_length=100)
    issue_date = models.DateField()
    department = models.TextField()
    rnn = models.TextField()
    citizenship = models.ForeignKey(Country)


#Основные Таблицы
class StudyYear(models.Model):
    year = models.IntegerField()


class Semester(models.Model):
    study_year = models.ForeignKey(StudyYear)
    order = models.IntegerField()


class Schedule(models.Model):
    semester = models.ForeignKey('Semester')
    academic_calendar_subject = models.ForeignKey('AcademicCalendarSubject')
    available_sets = models.IntegerField()


class ScheduleItem(models.Model):
    schedule = models.ForeignKey('Schedule')
    start_time = models.TimeField()
    end_time = models.TimeField()
    weekday = models.ForeignKey('Weekday')
    auditory = models.ForeignKey('Auditory')


class Announcement(models.Model):
    author = models.ForeignKey(User)
    date = models.DateField()
    text = models.TextField()


class Position(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class StudentMark(models.Model):
    theme_virtual_group = models.ForeignKey('ThemeVirtualGroup')
    student = models.ForeignKey('Student')


class MarkType(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Theme(models.Model):
    curriculum_subject = models.ForeignKey('CurriculumSubject')
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class VirtualGroup(models.Model):
    name = models.TextField()
    tutor_subject = models.ForeignKey('TutorSubject')
    course = models.IntegerField()
    schedule = models.OneToOneField('Schedule')


class Subject(models.Model):
    __metaclass__ = TransMeta
    code = models.CharField(max_length=100)
    name = models.TextField()
    cafedra = models.ForeignKey('Cafedra')

    class Meta:
        translate = ('name',)


class Building(models.Model):
    address = models.ForeignKey('Address')
    __metaclass__ = TransMeta
    information = models.TextField()

    class Meta:
        translate = ('information',)




class Auditory(models.Model):
    name = models.TextField()
    building = models.ForeignKey('Building')


class Cafedra(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()
    cafedra_head = models.ForeignKey('Tutor', related_name='cafedra_head')
    faculty = models.ForeignKey('Faculty')
    information = models.TextField()
    building = models.ForeignKey('Building')

    class Meta:
        translate = ('name', 'information',)


class Faculty(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()
    dean = models.ForeignKey('Tutor')
    information = models.TextField()

    class Meta:
        translate = ('name', 'information',)


class Specialty(models.Model):
    __metaclass__ = TransMeta
    code = models.CharField(max_length=100)
    name = models.TextField()
    faculty = models.ForeignKey('Faculty')
    information = models.TextField()

    class Meta:
        translate = ('name', 'information',)



class Direction(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()
    information = models.TextField()
    specialty = models.ForeignKey('Specialty')

    class Meta:
        translate = ('name', 'information',)


class Stream(models.Model):
    direction = models.OneToOneField('Direction')
    course = models.IntegerField()


class Group(models.Model):
    students = models.ManyToManyField('Student', related_name='groups')
    stream = models.ForeignKey('Stream')
    calendar = models.OneToOneField('AcademicCalendar', related_name='group')
    name = models.CharField(max_length=100)


class SubGroup(models.Model):
    index = models.IntegerField()
    group = models.ForeignKey('Group')
    prefect = models.OneToOneField('Student')


class AcademicCalendar(models.Model):
    pass


class RK(models.Model):
    calendar_subject = models.ForeignKey('AcademicCalendarSubject')
    start_date = models.DateField()
    end_date = models.DateField()


class Curriculum(models.Model):
    pass


#Роли
class SiteUser(models.Model):
    user = models.ForeignKey(User)
    patronymic = models.TextField()
    birth_date = models.DateField()
    birth_place = models.TextField()
    address = models.ForeignKey(Address)
    gender = models.ForeignKey(Gender)
    nationality = models.ForeignKey(Nationality)
    photo = models.ImageField(upload_to='photos')#avatar
    is_married = models.BooleanField()
    education = models.TextField()
    city = models.ForeignKey(City)
    id_card = models.ForeignKey('IDCard', null=True)
    role = models.ForeignKey('Role')
    language = models.ForeignKey('Language')


class Tutor(models.Model):
    __metaclass__ = TransMeta
    site_user = models.OneToOneField(SiteUser)
    cafedra = models.ForeignKey('Cafedra', null=True, blank=True)
    information = models.TextField()
    scientific_degree = models.ForeignKey(ScientificDegree)
    academic_degree = models.ForeignKey(AcademicDegree)
    rate = models.FloatField()
    status = models.ForeignKey(TutorStatus)
    start_date = models.DateField()
    length_of_work = models.IntegerField()
    job = models.ForeignKey('Job')

    class Meta:
        translate = ('information',)


class CafedraStaffTutor(models.Model):
    tutor = models.OneToOneField('Tutor')


class CafedraStaff(models.Model):
    site_user = models.OneToOneField(SiteUser)
    status = models.ForeignKey(StaffStatus)
    cafedra = models.ForeignKey('Cafedra')


class Parent(models.Model):
    user = models.ForeignKey(SiteUser)


class StudentActivity(models.Model):
    student = models.ForeignKey('Student')
    activity = models.ForeignKey('Activity')


class Student(models.Model):
    user = models.ForeignKey(SiteUser)
    parent = models.ForeignKey('Parent')
    study_form = models.ForeignKey('StudyForm')
    payment_form = models.ForeignKey('PaymentForm')
    study_language = models.ForeignKey('StudyLanguage')
    course = models.IntegerField()
    transcript = models.ForeignKey('Transcript')
    certificate_code = models.TextField()
    grant_code = models.TextField()
    ent_mark = models.IntegerField()
    current_gpa = models.FloatField()
    diplome_code = models.TextField()
    has_job = models.BooleanField()
    contract_code = models.TextField()
    status = models.ForeignKey(StudentStatus)


class Transcript(models.Model):
    series = models.TextField()
    number = models.TextField()
    given_date = models.DateField()


class TranscriptItem(models.Model):
    transcript = models.ForeignKey('Transcript')
    curriculum_subject = models.ForeignKey('CurriculumSubject')
    mark = models.IntegerField()

#Таблицы связывания


class SubGroupStudent(models.Model):
    sub_group = models.ForeignKey(SubGroup)
    student = models.ForeignKey(Student)


class StreamStudyYear(models.Model):
    stream = models.ForeignKey('Stream')
    study_year = models.ForeignKey('StudyYear')
    curriculum = models.OneToOneField('Curriculum')


class CurriculumSubject(models.Model):
    curriculum = models.ForeignKey('Curriculum')
    subject = models.ForeignKey('Subject')
    credits = models.IntegerField()
    semester = models.ForeignKey('Semester')
    hours = models.IntegerField()


class AcademicCalendarSubject(models.Model):
    calendar = models.ForeignKey('AcademicCalendar')
    curriculum_subject = models.ForeignKey('CurriculumSubject')
    start_date = models.DateField()
    end_date = models.DateField()


class Prerequisit(models.Model):
    parent_subject = models.ForeignKey('Subject', related_name='prerequisit_parent')
    depended_subject = models.ForeignKey('Subject', related_name='prerequisit_depended')


class Query(models.Model):
    student = models.ForeignKey('Student')
    tutor_schedule = models.ForeignKey('TutorSchedule')
    course = models.IntegerField()


class TutorSubject(models.Model):
    tutor = models.ForeignKey('Tutor')
    subject = models.ForeignKey('Subject')
    semester = models.ForeignKey('Semester')


class StudentVirtualGroup(models.Model):
    student = models.ForeignKey('Student')
    virtual_group = models.ForeignKey('VirtualGroup')


class ThemeVirtualGroup(models.Model):
    theme = models.ForeignKey('Theme')
    virtual_group = models.ForeignKey('VirtualGroup')
    start_date = models.DateField()
    end_date = models.DateField()


class Mark(models.Model):
    student_mark = models.ForeignKey('StudentMark')
    mark_type = models.ForeignKey('MarkType')


class ThemeMarkType(models.Model):
    mark_type = models.ForeignKey('MarkType')
    theme = models.ForeignKey('Theme')


class TutorSchedule(models.Model):
    tutor = models.ForeignKey('Tutor')
    schedule = models.ForeignKey('Schedule')


class PositionUser(models.Model):
    position = models.ForeignKey('Position')
    user = models.ForeignKey(User)


class AnnPosSpecialty(models.Model):
    announcement = models.ForeignKey('Announcement')
    position = models.ForeignKey('Position')
    specialty = models.ForeignKey('Specialty', null=True, blank=True)
    course = models.IntegerField(null=True, blank=True)


class Role(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class Permission(models.Model):
    __metaclass__ = TransMeta
    name = models.TextField()

    class Meta:
        translate = ('name',)


class RolePermission(models.Model):
    role = models.ForeignKey('Role')
    permission = models.ForeignKey('Permission')


class Settings(models.Model):
    current_semester = models.OneToOneField('Semester')
