#coding:utf-8
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from models import *
import forms
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse


def login_view(request):
    logout(request)
    context = RequestContext(request)
    form = forms.LoginForm(request.POST or None)
    errors = []
    context["form"] = form
    context["errors"] = errors
    context.update(csrf(request))

    is_valid = form.is_valid()
    if is_valid:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            site_user = user.siteuser_set.get()
            language_code = site_user.language.code
            request.session['django_language'] = language_code
            request.session['site_user'] = site_user
            return HttpResponseRedirect(reverse("main_index"))
        else:
            errors.append(u"Неправильный логин или пароль")
            return render_to_response("main/login.html", context)
    else:
        return render_to_response("main/login.html", context)


def logout_view(request):
    logout(request)
    context = RequestContext(request)
    return render_to_response("main/logout.html", context)


def index_view(request):
    context = RequestContext(request)
    return render_to_response("main/index.html", context)


def catalog_view(request):
    context = RequestContext(request)
    return render_to_response("main/catalog.html", context)


def change_language_view(request):
    context = RequestContext(request)
    context.update(csrf(request))
    form = forms.ChangeLanguageForm(request.POST or None)
    context["form"] = form

    if form.is_valid():
        language_id = int(request.POST['language'])
        language = Language.objects.get(id=language_id)
        request.session['django_language'] = language.code
        request.session['site_user'].language = language
        request.session['site_user'].save()
        return HttpResponseRedirect(reverse("main_language_changed"))

    return render_to_response("main/change_language.html", context)


def language_changed_view(request):
    context = RequestContext(request)
    return render_to_response("main/language_changed.html", context)


def faculty_list_view(request):
    context = RequestContext(request)
    faculties = Faculty.objects.all()
    for faculty in faculties:
        faculty.translated_name = faculty.name
    context["faculties"] = faculties
    return render_to_response("faculty/list.html", context)


def faculty_edit_view(request, faculty_id):
    context = RequestContext(request)
    faculty = Faculty.objects.select_related('dean').get(id=faculty_id)
    dean = faculty.dean
    messages = []

    faculty_name_en = faculty.name_en
    faculty_information_en = faculty.information_en
    faculty_name_ru = faculty.name_ru
    faculty_information_ru = faculty.information_ru
    faculty_name_kz = faculty.name_kz
    faculty_information_kz = faculty.information_kz

    form = forms.EditFacultyForm(
        request.POST or None,
        initial={
            "faculty_name_en": faculty_name_en,
            "faculty_information_en": faculty_information_en,
            "faculty_name_ru": faculty_name_ru,
            "faculty_information_ru": faculty_information_ru,
            "faculty_name_kz": faculty_name_kz,
            "faculty_information_kz": faculty_information_kz,
            "dean": faculty.dean_id
        }
    )

    if form.is_valid():
        faculty.name_en = request.POST['faculty_name_en']
        faculty.name_kz = request.POST['faculty_name_kz']
        faculty.name_ru = request.POST['faculty_name_ru']

        faculty.information_en = request.POST['faculty_information_en']
        faculty.information_kz = request.POST['faculty_information_kz']
        faculty.information_ru = request.POST['faculty_information_ru']

        faculty.dean = Tutor.objects.get(id=int(request.POST['dean']))
        faculty.save()
        messages.append("Saved")

    context["faculty_id"] = faculty_id
    context["form"] = form
    context["faculty_name"] = faculty.name
    context["messages"] = messages

    return render_to_response("faculty/edit.html", context)


def faculty_add_view(request):
    context = RequestContext(request)

    form = forms.EditFacultyForm(request.POST or None)
    messages = []

    if form.is_valid():
        name_en = request.POST['faculty_name_en']
        name_kz = request.POST['faculty_name_kz']
        name_ru = request.POST['faculty_name_ru']

        information_en = request.POST['faculty_information_en']
        information_kz = request.POST['faculty_information_kz']
        information_ru = request.POST['faculty_information_ru']

        dean = Tutor.objects.get(id=int(request.POST['dean']))

        Faculty.objects.create(
            name_en=name_en,
            name_ru=name_ru,
            name_kz=name_kz,
            information_en=information_en,
            information_kz=information_kz,
            information_ru=information_ru,
            dean=dean
        )
        messages.append("Successfully created")

    context["form"] = form
    context["messages"] = messages

    return render_to_response("faculty/add.html", context)


def faculty_delete_view(request, faculty_id):
    context = RequestContext(request)
    faculty = Faculty.objects.get(id=faculty_id)

    faculty_name = faculty.name

    if request.method == 'POST':
        if 'accept' in request.POST:
            faculty.delete()
            context['deleted'] = True
        if 'reject' in request.POST:
            return redirect('main_faculty_list')

    context["faculty_id"] = faculty_id
    context["faculty_name"] = faculty_name

    return render_to_response("faculty/delete.html", context)


def faculty_display_view(request, faculty_id):
    context = RequestContext(request)
    faculty = Faculty.objects.select_related('dean__site_user__user').get(id=faculty_id)
    dean = faculty.dean
    site_user = faculty.dean.site_user
    user = faculty.dean.site_user.user

    dean_name = user.last_name + ' ' + user.first_name + ' ' + site_user.patronymic
    dean_information = dean.information
    faculty_name = faculty.name
    faculty_information = faculty.information

    context["faculty_id"] = faculty_id
    context["faculty_name"] = faculty_name
    context["faculty_information"] = faculty_information
    context["dean_name"] = dean_name
    context["dean_information"] = dean_information

    return render_to_response("faculty/display.html", context)

