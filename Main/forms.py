#coding:utf-8
from django import forms
from models import Language, Tutor, Faculty
from django.utils.translation import ugettext as _

language_choices = dict(
    [(lang.id, lang.name) for lang in Language.objects.all()]
)


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, label=_("Login"))
    password = forms.CharField(widget=forms.PasswordInput, label=_("Password"))


class ChangeLanguageForm(forms.Form):
    language = forms.ChoiceField(choices=language_choices.items(), label=_("Language"))


class EditFacultyForm(forms.Form):
    faculty_name_en = forms.CharField(label=_("English faculty name"))
    faculty_name_ru = forms.CharField(label=_("Russian faculty name"))
    faculty_name_kz = forms.CharField(label=_("Kazakh faculty name"))

    faculty_information_en = forms.CharField(widget=forms.Textarea, label=_("English faculty information"))
    faculty_information_ru = forms.CharField(widget=forms.Textarea, label=_("Russian faculty information"))
    faculty_information_kz = forms.CharField(widget=forms.Textarea, label=_("Kazakh faculty information"))


    dean = forms.ChoiceField(
            choices=[
                (tutor.id,
                 tutor.site_user.user.last_name + ' ' +
                 tutor.site_user.user.first_name + ' ' +
                 tutor.site_user.patronymic) for tutor in Tutor.objects.all()

            ]
        )