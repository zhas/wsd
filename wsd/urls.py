from django.conf.urls import patterns, include, url
import Main
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^main/', include('Main.urls')),
                       url(r'^i18n/', include('django.conf.urls.i18n')),
                       )
